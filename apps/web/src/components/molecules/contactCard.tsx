import { Box, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import EditableLabel from '@components/atoms/editableLabel';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <EditableLabel name="name" defaultValue={name} onSave={console.log} />
          <EditableLabel
            name="email"
            defaultValue={email}
            onSave={console.log}
          />
        </Box>
      </Box>
    </Card>
  );
};
