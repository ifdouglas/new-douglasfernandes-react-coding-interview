import React, { useState, useEffect, useRef } from 'react';

interface EditableLabelProps {
  name: string;
  defaultValue: string;
  onSave?: (value: string) => void;
}

const EditableLabel = ({ name, defaultValue, onSave }: EditableLabelProps) => {
  const [isEditing, setIsEditing] = useState(false);
  const [value, setValue] = useState(defaultValue);
  const [originalValue, setOriginalValue] = useState(defaultValue);
  const inputRef = useRef(null);

  // Using inline styles to keep agnostic from any css framework
  const buttonStyle = {
    width: '26px',
    height: '22px',
    borderRadius: 0,
    borderColor: '#111',
    borderWidth: '1px',
    borderLeft: 'none',
  };

  const inputStyle = {
    height: '22px',
    borderColor: '#111',
    borderWidth: '1px',
    outline: 'none',
    borderBottomRightRadius: 0,
    borderTopRightRadius: 0,
  };

  const handleEdit = () => {
    setIsEditing(true);
  };

  const handleSave = () => {
    setIsEditing(false);

    // To keep the last edited value in case the user cancels
    setOriginalValue(value);

    if (onSave) {
      onSave(value);
    }
  };

  const handleCancel = () => {
    setValue(originalValue);
    setIsEditing(false);
  };

  useEffect(() => {
    if (isEditing) {
      inputRef.current.focus();
    }
  }, [isEditing]);

  return (
    <div>
      {isEditing ? (
        <>
          <input
            ref={inputRef}
            name={name}
            type="text"
            value={value}
            style={inputStyle}
            onChange={(e) => setValue(e.target.value)}
          />
          <button style={buttonStyle} onClick={handleSave}>
            ok
          </button>
          <button style={buttonStyle} onClick={handleCancel}>
            x
          </button>
        </>
      ) : (
        <>
          <input name={name} type="hidden" value={value} />
          <label onClick={handleEdit}>{value}</label>
        </>
      )}
    </div>
  );
};

export default EditableLabel;
